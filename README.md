smtp-relay
=========

Configure OpenBSD's [opensmtpd](https://www.opensmtpd.org/) daemon for relaying
emails via a SMTP server.

I use it to send emails via ProtonMail's bridge in my local LAN.

Requirements
------------

N/A

Role Variables
--------------

| Variable                             | Type     | Mandatory | Default                | Description                                                                                       |
|--------------------------------------|----------|-----------|------------------------|---------------------------------------------------------------------------------------------------|
| `osmtpd_config`                      | `dict`   | `true`    | `N/A`                  | Dictionary storing `smtpd(8)` configuration options                                               |
| `osmtpd_config['secrets']`           | `dict`   | `true`    | `N/A`                  | Dictionary storing options related to the secrets i.e., authentication creds, table               |
| `osmtpd_config['secrets']['table']`  | `string` | `true`    | `secrets`              | Name of OpenSMTPD [table](https://man.openbsd.org/table.5) to use for auth creds                  |
| `osmtpd_config['secrets']['file']`   | `string` | `true`    | `/etc/mail/secrets`    | Path to file storing authentication credentials for the secrets table                             |
| `osmtpd_config['relay']`             | `dict`   | `true`    | `N/A`                  | Dictionary storing configuration details related to the relay                                     |
| `osmtpd_config['relay']['table']`    | `string` | `true`    | `allowrelay`           | Name of OpenSMTPD table to use for hosts allowed to use the relay                                 |
| `osmtpd_config['relay']['file']`     | `string` | `true`    | `/etc/mail/allowrelay` | Path to file storing hosts allowed to use the relay                                               |
| `osmtpd_config['relay']['host']`     | `string` | `true`    | `N/A`                  | FQDN of relay host                                                                                |
| `osmtpd_config['relay']['port']`     | `string` | `true`    | `N/A`                  | Port the relay host is listening on for incoming data                                             |
| `osmtpd_config['relay']['tls']`      | `string` | `false`   | ` `                    | Per `smtpd.conf(5)`, use the optional `no-verify` value to skip cert validation                   |
| `osmtpd_config['relay']['mailfrom']` | `string` | `true`    | `N/A`                  | This is the `MAIL FROM` address the email will be sent with; relay must allow it                  |
| `osmtpd_config['listen']`            | `string` | `false`   | `all`                  | Name of interface the daemon listens on                                                           |
| `osmtpd_allowed_relay`               | `list`   | `true`    | `N/A`                  | List storing hosts allowed to use the relay via this host                                         |
| `osmtpd_allowed_relay[0]['host']`    | `string` | `true`    | `N/A`                  | IPv4 (or IPv6) network address of host allowed to use the relay                                   |
| `osmtpd_allowed_relay[0]['comment']` | `string` | `false`   | `N/A`                  | Comment to associate with entry in the table                                                      |
| `osmtpd_secrets`                     | `dict`   | `true`    | `N/A`                  | Dictionary storing authentication credentials used to populate `osmtpd_config['secrets']['file']` |
| `osmtpd_secrets['label']`            | `string` | `true`    | `N/A`                  | Label to use for the Credential table                                                             |
| `osmtpd_secrets['username']`         | `string` | `true`    | `N/A`                  | Username to use for authenticating to the relay                                                   |
| `osmtpd_secrets['password']`         | `string` | `true`    | `N/A`                  | Password to use for authenticating to the relay; might want to save it in an ansible-vault        |

### Examples ###

    osmtpd_config:
      secrets:
        file: /etc/mail/auth-creds
        table: authcreds
      relay:
        host: "relay-in.my.lan"
        port: "25"
        tls: "no-verify"
        mailfrom: "myemail@public-relay.example"
      listen: re0
    
    osmtpd_allowed_relay:
      - host: 172.16.4.1
        comment: "router"
      - host: 172.16.4.100
        comment: "monitoring system"

    osmtpd_secrets:
      label: myrelay
      username: "fred"
      password: "{{ vault_pass_for_fred }}"

This would create the `/etc/mail/auth-creds` file storing a single line made up
of `osmtpd_secrets` data i.e., credentials used to authenticate to the relay.  
The default `/etc/mail/allowrelay` file is used for storing hosts allowed to
use this host to reach the relay, with only `172.16.4.1` & `172.16.4.100` being
allowed.

Bugs/Caveats
------------

- TLS communication with the relay is required, though one can disable
  verification of presented certs
- Only one relay host can be configured
- Only one set of authentication credentials can be configured

Dependencies
------------

N/A

Example Playbook
----------------

Suppose one uses an `inventory.ini` inventory file that contains these hosts:

    [relay]
    internalrelay.my.lan

and a `opensmtpd.yml` playbook that looks like this:

    - hosts: relay
      become: true
      roles:
         - role: opensmtpd

one could call ansible like this:

    $ ansible-playbook -i inventory.ini opensmtpd.yml

License
-------

BSD

Author Information
------------------

[psa90](https://gitlab.com/psa90)
